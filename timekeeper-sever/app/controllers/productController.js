// Import thư viện mongoose
const mongoose = require("mongoose");
const productModel = require('../model/productModel');

// Create course
const createProduct = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let bodyRequest = req.body;

  // B2: Validate dữ liệu
  if (!bodyRequest.name) {
    return res.status(400).json({
      message: "product name is required!"
    })
  }

  if (!bodyRequest.imageUrl) {
    return res.status(400).json({
      message: "product imageUrl is required!"
    })
  }

  if (!bodyRequest.buyPrice) {
    return res.status(400).json({
      message: "product buyPrice is required!"
    })
  }

  if (!bodyRequest.promotionPrice) {
    return res.status(400).json({
      message: "product promotionPrice is required!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newProductData = {
    _id: mongoose.Types.ObjectId(),
    name: bodyRequest.name,
    description: bodyRequest.description,
    imageUrl: bodyRequest.imageUrl,
    buyPrice: bodyRequest.buyPrice,
    promotionPrice: bodyRequest.promotionPrice,
    amount: bodyRequest.amount,
  }

  productModel.create(newProductData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
        error: "something wrong when create product"
      })
    }

    return res.status(201).json({
      message: "Create successfully",
      newProduct: data
    })
  })
}

// Get all course
const getAllProducts = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  // B2: Validate dữ liệu
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  productModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Get all courses successfully",
      ProductTypes: data
    })
  })
}

// Get course by id
const getProductById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let productId = req.params.productId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      message: "Course ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  productModel.findById(productId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Get ProductType successfully",
      product: data
    })
  })
}

// Update course by id
const updateProduct = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let productId = req.params.productId;
  let bodyRequest = req.body;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).json({
      message: "ProductType ID is invalid!"
    })
  }

  // Bóc tách trường hợp undefied
  if (!bodyRequest.name) {
    return res.status(400).json({
      message: "Ma nuoc uong is required!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let productUpdate = {
    name: bodyRequest.name,
    description: bodyRequest.description,
    imageUrl: bodyRequest.imageUrl,
    buyPrice: bodyRequest.buyPrice,
    promotionPrice: bodyRequest.promotionPrice,
    amount: bodyRequest.amount,
  };

  productModel.findByIdAndUpdate(productId, productUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update course successfully",
      product: data
    })
  })
}

// Delete course by id
const deleteProduct = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let Productid = req.params.productId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(Productid)) {
    return res.status(400).json({
      message: "Product ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  productModel.findByIdAndDelete(Productid, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(204).json({
      message: "Delete product successfully"
    })
  })
}

// Export Course controller thành 1 module
module.exports = {
  createProduct,
  getAllProducts,
  getProductById,
  updateProduct,
  deleteProduct
}
