// Import thư viện mongoose
const mongoose = require("mongoose");
const customerModel = require('../model/customerModel');

// Create course
const createCustomer = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let bodyRequest = req.body;

  // B2: Validate dữ liệu
  if (!bodyRequest.fullName) {
    return res.status(400).json({
      message: "fullName is required!"
    })
  }

  if (!bodyRequest.phone) {
    return res.status(400).json({
      message: "phone is required!"
    })
  }

  if (!bodyRequest.email) {
    return res.status(400).json({
      message: "email is required!"
    })
  }

  if (!bodyRequest.address) {
    return res.status(400).json({
      message: "address is required!"
    })
  }

  if (!bodyRequest.city) {
    return res.status(400).json({
      message: "city is required!"
    })
  }

  if (!bodyRequest.country) {
    return res.status(400).json({
      message: "country is required!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newCustomerData = {
    _id: mongoose.Types.ObjectId(),
    fullName: bodyRequest.fullName,
    phone: bodyRequest.phone,
    email: bodyRequest.email,
    address: bodyRequest.address,
    city: bodyRequest.city,
    country: bodyRequest.country,
  }

  customerModel.create(newCustomerData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
        error: "something wrong when create product"
      })
    }

    return res.status(201).json({
      message: "Create successfully",
      newCustomer: data
    })
  })
}

// Get all course
const getAllCustomers = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  // B2: Validate dữ liệu
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  customerModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Get all courses successfully",
      customer: data
    })
  })
}

// Get course by id
const getCustomerById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let customerId = req.params.customerId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      message: "Course ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  customerModel.findById(customerId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Get Customer successfully",
      customer: data
    })
  })
}

// Update course by id
const updateCustomer = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let customerId = req.params.customerId;
  let bodyRequest = req.body;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      message: "ProductType ID is invalid!"
    })
  }

  // Bóc tách trường hợp undefied
  if (!bodyRequest.email) {
    return res.status(400).json({
      message: "Ma nuoc uong is required!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let customerUpdate = {
    fullName: bodyRequest.fullName,
    phone: bodyRequest.phone,
    email: bodyRequest.email,
    address: bodyRequest.address,
    city: bodyRequest.city,
    country: bodyRequest.country,
  };

  customerModel.findByIdAndUpdate(customerId, customerUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update course successfully",
      customer: data
    })
  })
}

// Delete course by id
const deleteCustomer = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let customerId = req.params.customerId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(customerId)) {
    return res.status(400).json({
      message: "Product ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  customerModel.findByIdAndDelete(customerId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(204).json({
      message: "Delete product successfully"
    })
  })
}

// Export Course controller thành 1 module
module.exports = {
  createCustomer,
  getAllCustomers,
  getCustomerById,
  updateCustomer,
  deleteCustomer
}
