// Import thư viện mongoose
const mongoose = require("mongoose");
const orderModel = require('../model/orderModel');

// Create course
const createOrder = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let bodyRequest = req.body;

  // B2: Validate dữ liệu
  console.log('da toi day3');


  if (!bodyRequest.note) {
    return res.status(400).json({
      message: "email is required!"
    })
  }

  if (!bodyRequest.cost) {
    return res.status(400).json({
      message: "address is required!"
    })
  }

  console.log('da toi day2');

  const date = new Date(bodyRequest.shippedDate)

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let newOrderData = {
    _id: mongoose.Types.ObjectId(),
    shippedDate: date,
    note: bodyRequest.note,
    cost: bodyRequest.cost,
  }

  console.log('da toi day1');

  orderModel.create(newOrderData, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message,
        error: "something wrong when create order"
      })
    }

    return res.status(201).json({
      message: "Create successfully",
      newOrder: data
    })
  })
}

// Get all course
const getAllOrders = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  // B2: Validate dữ liệu
  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  orderModel.find((error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Get all courses successfully",
      Order: data
    })
  })
}

// Get course by id
const getOrderById = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let orderId = req.params.orderId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      message: "Course ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  orderModel.findById(orderId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(201).json({
      message: "Get Order successfully",
      order: data
    })
  })
}

// Update course by id
const updateOrder = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let orderId = req.params.orderId;
  let bodyRequest = req.body;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      message: "ProductType ID is invalid!"
    })
  }

  // Bóc tách trường hợp undefied
  if (!bodyRequest.cost) {
    return res.status(400).json({
      message: "cost is required!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  let orderUpdate = {
    note: bodyRequest.note,
    cost: bodyRequest.cost,
  };

  orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(200).json({
      message: "Update course successfully",
      order: data
    })
  })
}

// Delete course by id
const deleteOrder = (req, res) => {
  // B1: Thu thập dữ liệu từ req
  let orderId = req.params.orderId;

  // B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(orderId)) {
    return res.status(400).json({
      message: "Product ID is invalid!"
    })
  }

  // B3: Gọi model thực hiện các thao tác nghiệp vụ
  orderModel.findByIdAndDelete(orderId, (error, data) => {
    if (error) {
      return res.status(500).json({
        message: error.message
      })
    }

    return res.status(204).json({
      message: "Delete order successfully"
    })
  })
}

// Export Course controller thành 1 module
module.exports = {
  createOrder,
  getAllOrders,
  getOrderById,
  updateOrder,
  deleteOrder
}
