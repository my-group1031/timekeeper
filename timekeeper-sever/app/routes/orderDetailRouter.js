//khai báo thư viện express
const express = require('express');
// const { OrderDetailMiddleware } = require('../middlewares/OrderDetailMiddleware');
// const Drink = require('../model/drinkModel');
// const OrderDetailController = require('../controllers/OrderDetailController');
//tạo router
const OrderDetailRouter = express.Router();

const {
  createOrderDetail,
  getAllOrderDetails,
  getOrderDetailById,
  updateOrderDetail,
  deleteOrderDetail
} = require('../controllers/orderDetatilController')


//sủ dụng middle ware
// OrderDetailRouter.use(OrderDetailMiddleware);


//create a OrderDetail
OrderDetailRouter.post('/orderDetails', createOrderDetail);

//get all OrderDetail
OrderDetailRouter.get('/orderDetails', getAllOrderDetails);

//get a OrderDetail
OrderDetailRouter.get('/orderDetails/:orderDetailId', getOrderDetailById)

//update a OrderDetail
OrderDetailRouter.put('/orderDetails/:orderDetailId', updateOrderDetail)

//delete a OrderDetail
OrderDetailRouter.delete('/orderDetails/:orderDetailId', deleteOrderDetail)

module.exports = { OrderDetailRouter };