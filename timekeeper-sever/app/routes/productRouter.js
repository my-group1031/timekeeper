//khai báo thư viện express
const express = require('express');
const { productMiddleware } = require('../middlewares/productMiddleware');
// const Drink = require('../model/drinkModel');
// const productController = require('../controllers/productController');
//tạo router
const ProductRouter = express.Router();

const {
  createProduct,
  getAllProducts,
  getProductById,
  updateProduct,
  deleteProduct
} = require('../controllers/productController')


//sủ dụng middle ware
ProductRouter.use(productMiddleware);


//create a productType
ProductRouter.post('/products', createProduct);

//get all productType
ProductRouter.get('/products', getAllProducts);

//get a productType
ProductRouter.get('/products/:productId', getProductById)

//update a productType
ProductRouter.put('/products/:productId', updateProduct)

//delete a productType
ProductRouter.delete('/products/:productId', deleteProduct)

module.exports = { ProductRouter };