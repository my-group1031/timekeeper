//khai báo thư viện express
const express = require('express');
// const { CustomerMiddleware } = require('../middlewares/CustomerMiddleware');
// const Drink = require('../model/drinkModel');
// const CustomerController = require('../controllers/CustomerController');
//tạo router
const CustomerRouter = express.Router();

const {
  createCustomer,
  getAllCustomers,
  getCustomerById,
  updateCustomer,
  deleteCustomer
} = require('../controllers/customerController')


//sủ dụng middle ware
// CustomerRouter.use(CustomerMiddleware);


//create a Customer
CustomerRouter.post('/customers', createCustomer);

//get all Customer
CustomerRouter.get('/customers', getAllCustomers);

//get a Customer
CustomerRouter.get('/customers/:customerId', getCustomerById)

//update a Customer
CustomerRouter.put('/customers/:customerId', updateCustomer)

//delete a Customer
CustomerRouter.delete('/customers/:customerId', deleteCustomer)

module.exports = { CustomerRouter };