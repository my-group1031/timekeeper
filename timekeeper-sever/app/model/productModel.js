const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Product = new Schema({
  // _id: {type: ObjectId, unique:true},
  name: { type: String, unique: true, required: true },
  description: { type: String },
  buyPrice: { type: Number, require: true },
  imageUrl: { type: String, require: true },
  promotionPrice: { type: Number, require: true },
  amount: { type: Number, default: 0 },
  productType: [
    {
      type: mongoose.Types.ObjectId,
      ref: "productType",
      required: true
    }
  ],
  timeCreated: { type: Date, default: Date.now() },
  timeUpdated: { type: Date, default: Date.now() },
}, {
  timestamps: true
});

module.exports = mongoose.model('Product', Product);
