const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Order = new Schema({
  // _id: {type: ObjectId, unique:true},
  orderDate: { type: Date, default: Date.now() },
  shippedDate: { type: Date },
  note: { type: String },
  cost: { type: Number, default: 0 },
  orderDetails: [
    {
      type: mongoose.Types.ObjectId,
      ref: "orderDetails",
    }
  ],
  timeCreated: { type: Date, default: Date.now() },
  timeUpdated: { type: Date, default: Date.now() },
}, {
  timestamps: true
});

module.exports = mongoose.model('Order', Order);
