//khai báo thư viện express
const express = require('express');
const { ProductTypeRouter } = require('./app/routes/productTypeRouter');
const { ProductRouter } = require('./app/routes/productRouter');
const { CustomerRouter } = require('./app/routes/customerRouter');
const { OrderRouter } = require('./app/routes/orderRouter');
const { OrderDetailRouter } = require('./app/routes/orderDetailRouter');


//khởi tạo ứng dụng nodejs
const app = new express();

//sử dụng được body json
app.use(express.json());

//sử dụng body unicode
app.use(express.urlencoded({
  extended: true
}))

//khai báo port chạy nodejs
const port = 8002;

const db = require('./config/db');
//Connect to DB
db.connect();

app.get('/', (request, response) => {
  let today = new Date();
  console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`);

  response.status(200).json({
    message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
  })
})

//sử dụng router
app.use('/', ProductTypeRouter);
app.use('/', ProductRouter);
app.use('/', CustomerRouter);
app.use('/', OrderRouter);
app.use('/', OrderDetailRouter);

app.listen(port, () => {
  console.log(`App chạy trên cổng ${port}`);
})