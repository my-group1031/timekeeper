import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import './App.scss';
import BlogPage from "./containers/BlogPage/BlogPage";
import HomePage from './containers/HomePage/HomePage';
import ProductDetailsPage from './containers/ProductDetailsPage/ProductDetailsPage';
import CartPage from './containers/CartPage/CartPage';
import ShopPage from "./containers/ShopPage/ShopPage";
import FormSignin from "./components/Form/FormSignin/FormSignin";


function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<HomePage />} />
        <Route exact path="/login" element={<FormSignin />} />
        <Route exact path="/products" element={<ShopPage />} />
        <Route path="/productDetails" element={<ProductDetailsPage />} />
        <Route path="/blog" element={<BlogPage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/contactus" element={<HomePage />} />
      </Routes>
    </Router>
  );
}

export default App;
