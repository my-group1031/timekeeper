import React from 'react';
import 'react-slideshow-image/dist/styles.css';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import ShoppingBagOutlinedIcon from '@mui/icons-material/ShoppingBagOutlined';
import Rating from '@mui/material/Rating';
import Stack from '@mui/material/Stack';

// import watch1 from '../../assets/images/watch1.png';
import watch1 from '../../assets/images/watches/watch1.png';
import watch2 from '../../assets/images/watches/watch2.png';
import watch3 from '../../assets/images/watches/watch17.png';
import watch4 from '../../assets/images/watches/watch4.png';
import watch5 from '../../assets/images/watches/watch5.png';
import watch6 from '../../assets/images/watches/watch6.png';
import watch7 from '../../assets/images/watches/watch7.png';
import watch8 from '../../assets/images/watches/watch8.png';

import './products.scss';

const dataProducts = [
  {
    tag: 'HOT',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch1,
  },
  {
    tag: '',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch2,
  },
  {
    tag: 'NEW',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch3,
  },
  {
    tag: 'HOT',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch4,
  },
  {
    tag: 'HOT',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch5,
  },
  {
    tag: 'SALE',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch6,
  },
  {
    tag: '',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch7,
  },
  {
    tag: 'HOT',
    title: 'Rolex Wristwatch',
    discription:
      'This is a wider card with supporting text below as a natural lead-in to additional content. ',
    oldPrice: '$960.0',
    newPrice: '$666.0',
    catagory: 'MEN.WATCH',
    image: watch8,
  },
];

function Products() {
  return (
    <div className="products">
      <div className="row">
        <div className="col-sm-12 text-center products-title">
          <h1>BEST SELLERS</h1>
          <p>
            LOREM IPSUM IS SIMPLY DUMMY TEXT OF THE PRINTING AND TYPESETTING
            INDUSTRY
          </p>
        </div>
      </div>
      <div className="products-best-sellers d-flex flex-wrap flex-row">
        {dataProducts.map((item, index) => (
          <div key={index} className="product-card">
            <div className="badge">{item.tag}</div>
            <div className="product-tumb">
              <img src={item.image} alt="" />
            </div>
            <div className="product-details">
              <span className="product-catagory">{item.catagory}</span>
              <h4>
                <a href="/#">{item.title}</a>
              </h4>
              <p>{item.discription}</p>
              <Stack spacing={1}>
                <Rating name="half-rating" defaultValue={2.5} precision={0.5} />
              </Stack>
              <div className="product-bottom-details">
                <div className="product-price">
                  <small>{item.oldPrice}</small>
                  {item.newPrice}
                </div>
                <div className="product-links">
                  <a href="/#">
                    <FavoriteBorderIcon className="icon-link icon-heart" />
                  </a>
                  <a href="/#">
                    <ShoppingBagOutlinedIcon className="icon-link icon-cart" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Products;
