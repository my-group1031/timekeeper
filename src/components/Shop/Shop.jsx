import React from 'react';
import Box from '@mui/joy/Box';
import Button from '@mui/joy/Button';
import Checkbox from '@mui/joy/Checkbox';
import List from '@mui/joy/List';
import ListItem from '@mui/joy/ListItem';
import Typography from '@mui/joy/Typography';
import Sheet from '@mui/joy/Sheet';
import Slider from '@mui/joy/Slider';
import { Grid, InputLabel, MenuItem, Select, FormControl } from '@mui/material';
import ViewModuleIcon from '@mui/icons-material/ViewModule';
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';

import './shop.scss';

// function valueLabelFormat(value) {
//   const units = ['KB', 'MB', 'GB', 'TB'];

//   let unitIndex = 0;
//   let scaledValue = value;

//   while (scaledValue >= 1024 && unitIndex < units.length - 1) {
//     unitIndex += 1;
//     scaledValue /= 1024;
//   }

//   return `${scaledValue} ${units[unitIndex]}`;
// }

function calculateValue(value) {
  return 10 * value;
}

function Shop() {
  const [status, setStatus] = React.useState({
    declinedPayment: true,
    deliveryError: true,
    wrongAddress: false,
  });

  const [age, setAge] = React.useState('');

  const handleSort = (event) => {
    setAge(event.target.value);
  };

  const [value, setValue] = React.useState(10);

  const handleChangePrice = (event, newValue) => {
    if (typeof newValue === 'number') {
      setValue(newValue);
    }
  };

  return (
    <Grid container className="shop">
      <Grid item xs={3} className="shop-filter px-3">
        <div className="filter-title">
          <h3>Filter products by</h3>
        </div>
        <div className="categories-filter filter-item">
          <Sheet
            // variant="outlined"
            sx={{ p: 2, borderRadius: 'sm', width: 300 }}
          >
            <Typography
              id="filter-status"
              sx={{
                textTransform: 'uppercase',
                fontSize: '1.3rem',
                letterSpacing: 'lg',
                fontWeight: '600',
                color: 'text.secondary',
                mb: 2,
              }}
            >
              Categories
            </Typography>
            <Box role="group" aria-labelledby="filter-status">
              <List>
                <ListItem variant="soft" color="danger">
                  <Checkbox
                    label="Digital Watch"
                    color="danger"
                    overlay
                    checked={status.declinedPayment}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        declinedPayment: event.target.checked,
                      })
                    }
                    sx={{ color: 'inherit' }}
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    8
                  </Typography>
                </ListItem>
                <ListItem
                  variant="plain"
                  color="warning"
                  sx={{ borderRadius: 'sm' }}
                >
                  <Checkbox
                    label="Men's Watches"
                    color="warning"
                    overlay
                    checked={status.deliveryError}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        deliveryError: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    24
                  </Typography>
                </ListItem>
                <ListItem variant="plain" sx={{ borderRadius: 'sm' }}>
                  <Checkbox
                    label="Women's Watches"
                    color="neutral"
                    overlay
                    checked={status.wrongAddress}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        wrongAddress: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    8
                  </Typography>
                </ListItem>
              </List>
            </Box>
            <Button
              className="btn-clear"
              variant="outlined"
              color="neutral"
              size="sm"
              onClick={() =>
                setStatus({
                  declinedPayment: false,
                  deliveryError: false,
                  wrongAddress: false,
                })
              }
              sx={{ px: 1.5, mt: 1 }}
            >
              Clear All
            </Button>
          </Sheet>
        </div>
        <div className="brand-filter filter-item mt-5">
          <Sheet
            // variant="outlined"
            sx={{ p: 2, borderRadius: 'sm', width: 300 }}
          >
            <Typography
              id="filter-status"
              sx={{
                textTransform: 'uppercase',
                fontSize: '1.3rem',
                letterSpacing: 'lg',
                fontWeight: '600',
                color: 'text.secondary',
                mb: 2,
              }}
            >
              Filter status
            </Typography>
            <Box role="group" aria-labelledby="filter-status">
              <List>
                <ListItem variant="soft" color="danger">
                  <Checkbox
                    label="Declined Payment"
                    color="danger"
                    overlay
                    checked={status.declinedPayment}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        declinedPayment: event.target.checked,
                      })
                    }
                    sx={{ color: 'inherit' }}
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    8
                  </Typography>
                </ListItem>
                <ListItem
                  variant="plain"
                  color="warning"
                  sx={{ borderRadius: 'sm' }}
                >
                  <Checkbox
                    label="Delivery Error"
                    color="warning"
                    overlay
                    checked={status.deliveryError}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        deliveryError: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    24
                  </Typography>
                </ListItem>
                <ListItem variant="plain" sx={{ borderRadius: 'sm' }}>
                  <Checkbox
                    label="Wrong Address"
                    color="neutral"
                    overlay
                    checked={status.wrongAddress}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        wrongAddress: event.target.checked,
                      })
                    }
                  />
                </ListItem>
              </List>
            </Box>
            <Button
              className="btn-clear"
              variant="outlined"
              color="neutral"
              size="sm"
              onClick={() =>
                setStatus({
                  declinedPayment: false,
                  deliveryError: false,
                  wrongAddress: false,
                })
              }
              sx={{ px: 1.5, mt: 1 }}
            >
              Clear All
            </Button>
          </Sheet>
        </div>

        <div className="price-filter filter-item mt-5">
          <Box sx={{ p: 2, borderRadius: 'sm', width: 290 }}>
            <Typography
              id="filter-status"
              sx={{
                textTransform: 'uppercase',
                fontSize: '1.3rem',
                letterSpacing: 'lg',
                fontWeight: '600',
                color: 'text.secondary',
                mb: 2,
              }}
            >
              Price
            </Typography>
            <Typography id="non-linear-slider" gutterBottom>
              USD: {calculateValue(value)} $
            </Typography>
            <Slider
              value={value}
              min={0}
              step={10}
              max={20000}
              scale={calculateValue}
              getAriaValueText={calculateValue}
              valueLabelFormat={calculateValue}
              onChange={handleChangePrice}
              valueLabelDisplay="auto"
              aria-labelledby="non-linear-slider"
            />
          </Box>
        </div>

        <div className="size-filter filter-item mt-5">
          <Sheet
            // variant="outlined"
            sx={{ p: 2, borderRadius: 'sm', width: 300 }}
          >
            <Typography
              id="filter-status"
              sx={{
                textTransform: 'uppercase',
                fontSize: '1.3rem',
                letterSpacing: 'lg',
                fontWeight: '600',
                color: 'text.secondary',
                mb: 2,
              }}
            >
              Size
            </Typography>
            <Box role="group" aria-labelledby="filter-status">
              <List>
                <ListItem variant="soft" color="danger">
                  <Checkbox
                    label="Declined Payment"
                    color="danger"
                    overlay
                    checked={status.declinedPayment}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        declinedPayment: event.target.checked,
                      })
                    }
                    sx={{ color: 'inherit' }}
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    28 mm
                  </Typography>
                </ListItem>
                <ListItem
                  variant="plain"
                  color="warning"
                  sx={{ borderRadius: 'sm' }}
                >
                  <Checkbox
                    label="Delivery Error"
                    color="warning"
                    overlay
                    checked={status.deliveryError}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        deliveryError: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    30 mm
                  </Typography>
                </ListItem>
                <ListItem
                  variant="plain"
                  color="warning"
                  sx={{ borderRadius: 'sm' }}
                >
                  <Checkbox
                    label="Delivery Error"
                    color="warning"
                    overlay
                    checked={status.deliveryError}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        deliveryError: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    32 mm
                  </Typography>
                </ListItem>
                <ListItem variant="plain" sx={{ borderRadius: 'sm' }}>
                  <Checkbox
                    label="Wrong Address"
                    color="neutral"
                    overlay
                    checked={status.wrongAddress}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        wrongAddress: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    40 mm
                  </Typography>
                </ListItem>
              </List>
            </Box>
            <Button
              className="btn-clear"
              variant="outlined"
              color="neutral"
              size="sm"
              onClick={() =>
                setStatus({
                  declinedPayment: false,
                  deliveryError: false,
                  wrongAddress: false,
                })
              }
              sx={{ px: 1.5, mt: 1 }}
            >
              Clear All
            </Button>
          </Sheet>
        </div>

        <div className="color-filter filter-item mt-5">
          <Sheet
            // variant="outlined"
            sx={{ p: 2, borderRadius: 'sm', width: 300 }}
          >
            <Typography
              id="filter-status"
              sx={{
                textTransform: 'uppercase',
                fontSize: '1.3rem',
                letterSpacing: 'lg',
                fontWeight: '600',
                color: 'text.secondary',
                mb: 2,
              }}
            >
              Color
            </Typography>
            <Box role="group" aria-labelledby="filter-status">
              <List>
                <ListItem variant="soft" color="danger">
                  <Checkbox
                    label="Declined Payment"
                    color="danger"
                    overlay
                    checked={status.declinedPayment}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        declinedPayment: event.target.checked,
                      })
                    }
                    sx={{ color: 'inherit' }}
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    Black
                  </Typography>
                </ListItem>
                <ListItem
                  variant="plain"
                  color="warning"
                  sx={{ borderRadius: 'sm' }}
                >
                  <Checkbox
                    label="Delivery Error"
                    color="warning"
                    overlay
                    checked={status.deliveryError}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        deliveryError: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    Blue
                  </Typography>
                </ListItem>
                <ListItem
                  variant="plain"
                  color="warning"
                  sx={{ borderRadius: 'sm' }}
                >
                  <Checkbox
                    label="Delivery Error"
                    color="warning"
                    overlay
                    checked={status.deliveryError}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        deliveryError: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    Brown
                  </Typography>
                </ListItem>
                <ListItem variant="plain" sx={{ borderRadius: 'sm' }}>
                  <Checkbox
                    label="Wrong Address"
                    color="neutral"
                    overlay
                    checked={status.wrongAddress}
                    onChange={(event) =>
                      setStatus({
                        ...status,
                        wrongAddress: event.target.checked,
                      })
                    }
                  />
                  <Typography textColor="inherit" sx={{ ml: 'auto' }}>
                    White
                  </Typography>
                </ListItem>
              </List>
            </Box>
            <Button
              className="btn-clear"
              variant="outlined"
              color="neutral"
              size="sm"
              onClick={() =>
                setStatus({
                  declinedPayment: false,
                  deliveryError: false,
                  wrongAddress: false,
                })
              }
              sx={{ px: 1.5, mt: 1 }}
            >
              Clear All
            </Button>
          </Sheet>
        </div>
      </Grid>
      <Grid item xs={8} sx={{ marginLeft: '30px' }} className="shop-products">
        <div className="row product-layout">
          <div className="product-layout-button">
            <ViewModuleIcon />
            <FormatListBulletedIcon />
          </div>
          <div className="product-layout-sort"></div>
        </div>
      </Grid>
    </Grid>
  );
}

export default Shop;
