import React from 'react';
import { Link } from 'react-router-dom';

import './titlePage.scss';

function TitlePage({ title, breadCrumd }) {
  return (
    <div className="title-page">
      <div className="bg-title-page"></div>
      <nav data-depth="3" className="breadcrumb">
        <div className="breadcrumb-container">
          <div className="title text-center">
            <h3>{title}</h3>
          </div>

          <ul itemScope="" itemType="http://schema.org/BreadcrumbList">
            {breadCrumd.map((item, index) => (
              <li
                key={index}
                itemProp="itemListElement"
                itemScope=""
                itemType="http://schema.org/ListItem"
              >
                <span itemProp="name">
                  <Link to="/">{item}</Link>
                </span>
                <meta itemProp="position" content="1" />
              </li>
            ))}
            {/* <li
              itemProp="itemListElement"
              itemScope=""
              itemType="http://schema.org/ListItem"
            >
              <a
                itemProp="item"
                href="http://ps.magentech.com/themes/sp_time/en/13-accessories"
              >
                <span itemProp="name">Accessories</span>
              </a>
              <meta itemProp="position" content="2" />
            </li>
            <li
              itemProp="itemListElement"
              itemScope=""
              itemType="http://schema.org/ListItem"
            >
              <a
                itemProp="item"
                href="http://ps.magentech.com/themes/sp_time/en/accessories/49-353-copy-of-duis-malesuada-urna-euismod.html#/size-28_mm/color-blue"
              >
                <span itemProp="name">
                  Curabitur molestie tellus Spirit Of Big Bang suoit
                </span>
              </a>
              <meta itemProp="position" content="3" />
            </li> */}
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default TitlePage;
