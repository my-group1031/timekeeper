import React from 'react';
import { useEffect, useState } from 'react';
import {
  GoogleAuthProvider,
  signInWithPopup,
  signOut,
  onAuthStateChanged,
} from '../../../../node_modules/firebase/auth';
import auth from '../../../firebase';

import './formSignin.scss';

const provider = new GoogleAuthProvider();

function FormSignin() {
  const [user, setUser] = useState(null);

  const loginGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        console.log(result);
        setUser(result.user);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const logoutGoogle = () => {
    signOut(auth)
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      if (result) {
        setUser(result);
      } else {
        setUser(null);
      }
    });
  }, []);

  return (
    <div class="login-page">
      <div class="form">
        <form class="register-form">
          <input type="text" placeholder="name" />
          <input type="password" placeholder="password" />
          <input type="text" placeholder="email address" />
          <button>create</button>
          <p class="message">
            Already registered? <a href="#">Sign In</a>
          </p>
        </form>
        <form class="login-form">
          <input type="text" placeholder="username" />
          <input type="password" placeholder="password" />
          <button>login</button>
          <hr />
          {user ? (
            <>
              <h4>Hi, {user.displayName}</h4>
              <img
                src={user.photoURL}
                style={{ width: '50px', borderRadius: '50%' }}
                alt="avatar"
              ></img>
              <br></br>
              <button className="loginGoogle" onClick={logoutGoogle}>
                Sign out
              </button>
            </>
          ) : (
            <button className="loginGoogle" onClick={loginGoogle}>
              Sign in with Google
            </button>
          )}

          <p class="message">
            Not registered? <a href="#">Create an account</a>
          </p>
        </form>
      </div>
    </div>
  );
}

export default FormSignin;
