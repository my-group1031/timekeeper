import {
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
} from '@mui/material';
import React from 'react';

import './cartItem.scss';
import watch24 from '../../assets/images/watches/watch24.png';
import { Table } from 'react-bootstrap';

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}
const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

function CartItem() {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="left">Dessert (100g serving)</TableCell>
            <TableCell align="center">Calories</TableCell>
            <TableCell align="center">Fat&nbsp;(g)</TableCell>
            <TableCell align="center">Carbs&nbsp;(g)</TableCell>
            <TableCell align="center">Protein&nbsp;(g)</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell component="th" scope="row">
              <div
                className="product-line-grid-left"
                style={{ width: '100px' }}
              >
                <span className="product-image media-middle">
                  <img
                    src={watch24}
                    alt="Vestibulum vehicula accumsan velit et euismod aptent"
                  />
                </span>
              </div>
            </TableCell>
            <TableCell align="left">
              <div className="product-line-grid-body">
                <div className="product-line-info">
                  <a className="label" href="#/" data-id_customization="0">
                    Vestibulum vehicula accumsan velit et euismod aptent
                  </a>
                </div>

                <div className="product-line-info product-price h5 ">
                  <div className="current-price">
                    <span className="price">$ 661.00</span>
                  </div>
                </div>

                <div className="product-line-info">
                  <span className="label">Size:&nbsp;</span>
                  <span className="value">28 mm </span>
                </div>
                <div className="product-line-info">
                  <span className="label">Color:&nbsp;</span>
                  <span className="value">Beige</span>
                </div>
              </div>
            </TableCell>
            <TableCell align="center">
              <TextField id="outlined-number" label="Number" type="number" />
            </TableCell>
            <TableCell align="center">
              <span className="product-price">
                <strong>$ 661.00</strong>
              </span>
            </TableCell>
            <TableCell align="center">
              <div className="cart-line-product-actions">
                <a
                  className="remove-from-cart"
                  rel="nofollow"
                  href="http://ps.magentech.com/themes/sp_time/en/cart?delete=1&amp;id_product=47&amp;id_product_attribute=368&amp;token=ec8148a277c44504af051f7c89dd6eda"
                  data-link-action="delete-from-cart"
                  data-id-product="47"
                  data-id-product-attribute="368"
                  data-id-customization=""
                >
                  <i className="material-icons float-xs-left">delete</i>
                </a>
              </div>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>

    // <div className="col-md-8">
    //   <ul className="">
    //     <li className="cart-item ">
    //       <div className="product-line-grid my-3 d-flex align-items-center">
    //         {/* <!--  product left content: image--> */}
    //         <div className="product-line-grid-left col-md-3 col-xs-4">
    //           <span className="product-image media-middle">
    //             <img
    //               src={watch24}
    //               alt="Vestibulum vehicula accumsan velit et euismod aptent"
    //             />
    //           </span>
    //         </div>

    //         {/* <!--  product left body: description --> */}
    //         <div className="product-line-grid-body col-md-4 col-xs-8">
    //           <div className="product-line-info">
    //             <a className="label" href="#/" data-id_customization="0">
    //               Vestibulum vehicula accumsan velit et euismod aptent
    //             </a>
    //           </div>

    //           <div className="product-line-info product-price h5 ">
    //             <div className="current-price">
    //               <span className="price">$ 661.00</span>
    //             </div>
    //           </div>

    //           <div className="product-line-info">
    //             <span className="label">Size:&nbsp;</span>
    //             <span className="value">28 mm </span>
    //           </div>
    //           <div className="product-line-info">
    //             <span className="label">Color:&nbsp;</span>
    //             <span className="value">Beige</span>
    //           </div>
    //         </div>

    //         {/* <!--  product left body: description --> */}
    //         <div className="product-line-grid-right product-line-actions  col-md-5 col-xs-12">
    //           <div className="row d-flex align-items-center">
    //             <div className="col-xs-4 hidden-md-up"></div>
    //             <div className="col-md-10 col-xs-6">
    //               <div className="row d-flex align-items-center">
    //                 <div className="col-md-6 col-xs-6 qty ">
    //                   <TextField
    //                     id="outlined-number"
    //                     label="Number"
    //                     type="number"
    //                   />
    //                 </div>
    //                 <div className="col-md-6 col-xs-2 price">
    //                   <span className="product-price">
    //                     <strong>$ 661.00</strong>
    //                   </span>
    //                 </div>
    //               </div>
    //             </div>
    //             <div className="col-md-2 col-xs-2 text-xs-right">
    //               <div className="cart-line-product-actions">
    //                 <a
    //                   className="remove-from-cart"
    //                   rel="nofollow"
    //                   href="http://ps.magentech.com/themes/sp_time/en/cart?delete=1&amp;id_product=47&amp;id_product_attribute=368&amp;token=ec8148a277c44504af051f7c89dd6eda"
    //                   data-link-action="delete-from-cart"
    //                   data-id-product="47"
    //                   data-id-product-attribute="368"
    //                   data-id-customization=""
    //                 >
    //                   <i className="material-icons float-xs-left">delete</i>
    //                 </a>
    //               </div>
    //             </div>
    //           </div>
    //         </div>

    //         <div className="clearfix"></div>
    //       </div>
    //     </li>
    //   </ul>
    // </div>
  );
}

export default CartItem;
