import React, { useState, useEffect } from 'react';
import {
  AppBar,
  Typography,
  Box,
  Menu,
  MenuItem,
  Grid,
  FormControl,
  InputLabel,
  Select,
  Badge,
} from '@mui/material';
import { Stack } from '@mui/system';
import EuroIcon from '@mui/icons-material/Euro';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import TemporaryDrawer from '../../../materialUI/TemporaryDrawer';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

import './header.scss';
import phone from '../../../assets/images/phone.png';
import cart from '../../../assets/images/cart.png';
import userSignin from '../../../assets/images/user.png';
import { Link } from 'react-router-dom';

import {
  GoogleAuthProvider,
  signInWithPopup,
  signOut,
  onAuthStateChanged,
} from '../../../../node_modules/firebase/auth';
import auth from '../../../firebase';

const provider = new GoogleAuthProvider();

function Header() {
  const [userLogin, setUserLogin] = useState(null);

  const loginGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        console.log(result);
        setUserLogin(result.user);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const logoutGoogle = () => {
    signOut(auth)
      .then(() => {
        setUserLogin(null);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      if (result) {
        setUserLogin(result);
      } else {
        setUserLogin(null);
      }
    });
  }, []);

  const [open, setOpen] = useState(false);
  const [user, setUser] = React.useState('');
  const [eur, setEur] = React.useState('');
  const [language, setLanguage] = React.useState('');

  const handleChangeUser = (event) => {
    setUser(event.target.value);
  };

  const handleChangeEur = (event) => {
    setEur(event.target.value);
  };

  const handleChangeLanguage = (event) => {
    setLanguage(event.target.value);
  };

  return (
    <header>
      {/* PC - tablet */}
      <div className="desktop-screen">
        <AppBar
          position="sticky"
          className="nav-top"
          style={{ background: '#ffffff' }}
        >
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={4}>
              <Typography
                sx={{
                  mx: 3,
                  my: 3,
                  display: { xs: 'none', md: 'flex' },
                  fontFamily: 'monospace',
                  fontWeight: 700,
                  letterSpacing: '.3rem',
                  color: 'inherit',
                  textDecoration: 'none',
                }}
              >
                <Link to="/">
                  <img
                    className="logo img-responsive"
                    src="http://ps.magentech.com/themes/sp_time/img/logo.png"
                    alt="SP Time"
                  />
                </Link>
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Stack
                direction="row"
                justifyContent="flex-end"
                alignItems="center"
              >
                <Box>
                  <FormControl
                    sx={{ m: 1, minWidth: 90, outLine: 'none' }}
                    size="small"
                  >
                    <InputLabel
                      fontSize="10px"
                      id="demo-simple-select-label"
                      fontFamily="montserrat"
                      style={{ fontFamily: 'montserrat', fontSize: '14px' }}
                    >
                      USER
                    </InputLabel>
                    <Select
                      style={{
                        boxShadow:
                          'rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
                      }}
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={user}
                      label="Age"
                      onChange={handleChangeUser}
                    >
                      <MenuItem value={10}>Login</MenuItem>
                      <MenuItem value={20}>Register</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
                <Box>
                  <FormControl sx={{ m: 1, minWidth: 90 }} size="small">
                    <InputLabel
                      fontSize="10px"
                      id="demo-simple-select-label"
                      style={{ fontFamily: 'montserrat', fontSize: '14px' }}
                    >
                      EUR
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={eur}
                      label="Age"
                      onChange={handleChangeEur}
                      style={{
                        boxShadow:
                          'rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
                      }}
                    >
                      <MenuItem value={10}>USD</MenuItem>
                      <MenuItem value={20}>EUR</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
                <Box>
                  <FormControl sx={{ m: 1, minWidth: 90 }} size="small">
                    <InputLabel
                      fontSize="10px"
                      id="demo-simple-select-label"
                      style={{ fontFamily: 'montserrat', fontSize: '14px' }}
                    >
                      LAN
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={language}
                      label="Age"
                      onChange={handleChangeLanguage}
                      style={{
                        boxShadow:
                          'rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
                      }}
                    >
                      <MenuItem value={10}>English</MenuItem>
                      <MenuItem value={20}>Tiếng việt</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
              </Stack>
            </Grid>
            <Grid item xs={4}>
              <Stack flexDirection="row" justifyContent="space-evenly">
                <Stack
                  flexDirection="row"
                  justifyContent="center"
                  alignItems="center"
                >
                  <Box
                    component="a"
                    sx={{
                      mr: 2,
                      my: 3,
                      display: { xs: 'none', md: 'flex' },
                      color: 'inherit',
                      textDecoration: 'none',
                    }}
                  >
                    <img src={phone} alt="phone" />
                  </Box>
                  <Box>
                    {' '}
                    <Typography
                      color="initial"
                      fontSize="16px"
                      fontFamily="philosopher"
                    >
                      Call us free
                    </Typography>
                    <Typography
                      color="#cf9e18"
                      fontSize="18px"
                      fontWeight="700"
                      fontFamily="philosopher"
                    >
                      1-888-546-789
                    </Typography>
                  </Box>
                </Stack>
                <Stack>
                  <Stack
                    flexDirection="row"
                    justifyContent="center"
                    alignItems="center"
                    component="a"
                    href="/#"
                    sx={{
                      textDecoration: 'none',
                    }}
                  >
                    <Box
                      component="a"
                      sx={{
                        mr: 2,
                        my: 3,
                        display: { xs: 'none', md: 'flex' },
                        color: 'inherit',
                        textDecoration: 'none',
                      }}
                    >
                      <Badge badgeContent={9} color="warning">
                        <img src={cart} alt="cart" />
                      </Badge>
                    </Box>
                    <Box>
                      <Typography
                        color="initial"
                        fontSize="16px"
                        fontFamily="philosopher"
                      >
                        Your cart
                      </Typography>
                      <Typography
                        color="#cf9e18"
                        fontSize="18px"
                        fontWeight="700"
                        fontFamily="philosopher"
                      >
                        {/* <EuroIcon style={{ fontSize: '18px' }} /> */}
                        <span>$</span>
                        0.00
                      </Typography>
                    </Box>
                  </Stack>
                </Stack>
                <Stack>
                  <Stack
                    flexDirection="row"
                    justifyContent="center"
                    alignItems="center"
                    component="a"
                    href="/#"
                    sx={{
                      textDecoration: 'none',
                    }}
                  >
                    {userLogin ? (
                      <>
                        <Box
                          component="span"
                          onClick={logoutGoogle}
                          sx={{
                            mr: 2,
                            my: 3,
                            display: { xs: 'none', md: 'flex' },
                            color: 'inherit',
                            textDecoration: 'none',
                            width: '40px',
                          }}
                        >
                          <img
                            style={{
                              borderRadius: '9999px',
                            }}
                            src={userLogin.photoURL}
                            alt="avatar user"
                          />
                        </Box>
                        <Box>
                          <Typography
                            color="initial"
                            fontSize="16px"
                            fontFamily="philosopher"
                          >
                            Hi!
                          </Typography>
                          <Typography
                            color="#cf9e18"
                            fontSize="18px"
                            fontWeight="700"
                            fontFamily="philosopher"
                          >
                            {userLogin.displayName}
                          </Typography>
                        </Box>
                      </>
                    ) : (
                      <Box
                        component="span"
                        onClick={loginGoogle}
                        sx={{
                          mr: 2,
                          my: 3,
                          display: { xs: 'none', md: 'flex' },
                          color: 'inherit',
                          textDecoration: 'none',
                          width: '36px',
                        }}
                      >
                        <img src={userSignin} alt="phone" />
                      </Box>
                    )}
                  </Stack>
                </Stack>
              </Stack>
            </Grid>
          </Grid>

          <Menu
            id="demo-positioned-menu"
            aria-labelledby="demo-positioned-button"
            open={open}
            onClose={(e) => setOpen(false)}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
          >
            <MenuItem>Profile</MenuItem>
            <MenuItem>My account</MenuItem>
            <MenuItem>Logout</MenuItem>
          </Menu>
        </AppBar>

        <nav className="nav-bottom navbar navbar-expand-lg bg-dark">
          <div className="container-fluid">
            <button
              className="navbar-toggler text-light bg-light"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="nav-list justify-content-between flex-wrap collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav justify-content-evenly mb-2 mb-lg-0">
                <li className="nav-item">
                  {/* <a className="nav-link active" aria-current="page" href="/#">
                    Home
                  </a> */}
                  <Link className="nav-link" to="/">
                    Home
                  </Link>
                </li>
                <li className="nav-item nav-link-shop">
                  {/* <a className="nav-link active" aria-current="page" href="/#">
                    Home
                  </a> */}
                  <Link className="nav-link " to="/products">
                    Shop
                  </Link>
                </li>
                <li className="nav-item dropdown nav-link-dropdown">
                  {/* <a
                    className="nav-link dropdown-toggle"
                    href="/#"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    aria-current="page"
                  >
                    Shop
                  </a> */}

                  <Link
                    className="nav-link dropdown-toggle"
                    to="/products"
                    role="button"
                    data-bs-toggle="dropdown"
                    aria-expanded="false"
                    aria-current="page"
                  ></Link>

                  <ul className="dropdown-menu">
                    <li>
                      <a className="dropdown-item" href="/#">
                        Men Watch
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item" href="/#">
                        Women Watch
                      </a>
                    </li>
                    <li>
                      <hr className="dropdown-divider"></hr>
                    </li>
                    <li>
                      <a className="dropdown-item" href="/#">
                        Digital Watch
                      </a>
                    </li>
                  </ul>
                </li>
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="/#">
                    Specials
                  </a>
                </li>
                {/* <li>Dashboard</li> */}{' '}
                <li className="nav-item">
                  {/* <a className="nav-link active" aria-current="page" href="/#">
                    Blog
                  </a> */}
                  <Link className="nav-link" to="/blog">
                    Blog
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="/#">
                    Contact Us
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link active" aria-current="page" href="/#">
                    About US
                  </a>
                </li>
              </ul>
              <form className="d-flex" role="search">
                <div className="search__container">
                  <input
                    className="search__input"
                    type="text"
                    placeholder="Search products"
                  />
                </div>
              </form>
            </div>
          </div>
        </nav>
      </div>

      {/* Mobile */}
      <div className="mobile-screen">
        <div className="header-top-mobile d-flex justify-content-between align-items-center py-3 px-3">
          <div className="header-icon-menu">
            <TemporaryDrawer />
          </div>
          <div className="header-logo">
            <a href="http://ps.magentech.com/themes/sp_time/">
              <img
                style={{ width: '90px' }}
                src="http://ps.magentech.com/themes/sp_time/modules/spthemeconfigurator//patterns/logo-mobile-1.png"
                alt="SP Time"
              />
            </a>
          </div>
          <div className="header-cart">
            <ShoppingCartOutlinedIcon
              style={{ fontSize: '33px', color: 'white' }}
            />
          </div>
        </div>
        <div className="header-bottom-mobile d-flex align-items-center">
          <form className="search-form">
            <input
              type="search"
              placeholder="Search"
              className="search-input"
            />
            <button type="submit" className="search-button">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="#000000"
                viewBox="0 0 30 30"
                width="30px"
                height="30px"
              >
                <path d="M 13 3 C 7.4889971 3 3 7.4889971 3 13 C 3 18.511003 7.4889971 23 13 23 C 15.396508 23 17.597385 22.148986 19.322266 20.736328 L 25.292969 26.707031 A 1.0001 1.0001 0 1 0 26.707031 25.292969 L 20.736328 19.322266 C 22.148986 17.597385 23 15.396508 23 13 C 23 7.4889971 18.511003 3 13 3 z M 13 5 C 17.430123 5 21 8.5698774 21 13 C 21 17.430123 17.430123 21 13 21 C 8.5698774 21 5 17.430123 5 13 C 5 8.5698774 8.5698774 5 13 5 z" />
              </svg>
            </button>
          </form>
        </div>
      </div>
    </header>
  );
}

export default Header;
