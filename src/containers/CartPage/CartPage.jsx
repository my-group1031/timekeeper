import React from 'react';

import CartItem from '../../components/CartItem/CartItem';
import Header from '../../components/Layout/Header/Header';
import Footer from '../../components/Layout/Footer/Footer';

function CartPage() {
  return (
    <div className="card-page">
      <Header />
      <div className="shopping-cart">
        <div className="title"></div>
        <CartItem />
      </div>
      <Footer />
    </div>
  );
}

export default CartPage;
