import React from 'react';
import Footer from '../../components/Layout/Footer/Footer';
import Header from '../../components/Layout/Header/Header';
import ProductDetail from '../../components/ProductDetail/ProductDetail';
import TitlePage from '../../components/TitlePage/TitlePage';

const breadCrumd = [
  'Home',
  'Accessories',
  'Curabitur molestie tellus Spirit Of Big Bang suoit',
];

function BlogPage() {
  return (
    <div>
      <Header />
      <TitlePage title="Blog" breadCrumd={breadCrumd} />

      <Footer />
    </div>
  );
}

export default BlogPage;
