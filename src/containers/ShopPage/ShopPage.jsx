import React from 'react';
import Footer from '../../components/Layout/Footer/Footer';
import Header from '../../components/Layout/Header/Header';
import Shop from '../../components/Shop/Shop';
import TitlePage from '../../components/TitlePage/TitlePage';

const breadCrumb = ['Home'];
function ShopPage() {
  return (
    <div>
      <Header />
      <TitlePage title={'Shop'} breadCrumd={breadCrumb} />
      <Shop />
      <Footer />
    </div>
  );
}

export default ShopPage;
