import React from 'react';
import Footer from '../../components/Layout/Footer/Footer';
import Header from '../../components/Layout/Header/Header';
import ProductDetail from '../../components/ProductDetail/ProductDetail';
import TitlePage from '../../components/TitlePage/TitlePage';

function ProductDetailsPage() {
  return (
    <div className="product-details">
      <Header />
      <TitlePage title="Product Detail" />
      <ProductDetail />
      <Footer />
    </div>
  );
}

export default ProductDetailsPage;
